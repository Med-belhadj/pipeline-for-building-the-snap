pipeline{
    // Here we define the agent for the pipeline : Execute the Pipeline, or stage, on any available agent.
    // agent any
        agent {
        node {
            label 'xenial1 '
        }
    }
    // Add the parameters for the Snap (provider, snap vendor, snap name, snap-clean ..) 
    parameters {
        choice(name: 'SNAP_VEND', choices: ['oai', 'srs', 'amarisoft'], description: 'Pick the provider value')
        choice(name: 'PROVIDER', choices: ['lxd', 'multipass', 'host'], description: 'Pick the provider value')
        booleanParam(name: 'SNAP_CLEAN', defaultValue: true, description: 'Toggle this value')
        choice(name: 'SNAP_NAME', choices: ['oai-enb', 'oai-hss', 'oai-gnb','oai-mme','oai-spgwc','oai-spgwu','flexran','oai-cdb','srs-ran','amari-ran'], description: 'Pick the element that you want to build')
    } 
    // Define the variables required for the Snap (Name, Vendor, Path..)
    environment {
        //SNAP_NAME="oai-hss"
        //SNAP_VEND_1="oai"
        //SNAP_VEND_2="srs"
        //SNAP_VEND_3="amarisoft"
        //SNAP_FULL_1="$SNAP_VEND_1/$SNAP_NAME"
        //SNAP_FULL_2="$SNAP_VEND_2/$SNAP_NAME"
        SNAP_FULL="$SNAP_VEND/$SNAP_NAME"
        SNAP_PATH="$WORKSPACE/snap/$SNAP_FULL" 
        //SNAP_PATH_2="$WORKSPACE/snap/$SNAP_FULL_2" //this is the path for the srs vendor
        //SNAP_PATH_3="$WORKSPACE/snap/$SNAP_FULL_3" //this is the path for the amarisoft  vendor
        //SNAPCRAFT_PARTS="hss cassandra frontend backend extensions"
    }
    // This pipeline will contain 10 stages grouped here, that will run sequentially
    // Each stage itself contains a sequence of steps that may contain some scripts to run
    stages {
        // STAGE 1: Print the values of parameters defined at the top-level of the pipeline block
        stage('Pipeline-Testing') {
            steps {
                echo "last commit"
                echo "SNAP_VEND : is setted to  $SNAP_VEND"
                echo "SNAP_NAME : is setted to  $SNAP_NAME"
                echo " SNAP_FULL : $SNAP_FULL"
                echo "The Snap path  : $SNAP_PATH"
                echo "The PROVIDER : ${params.PROVIDER}"
                echo "Current workspace is $WORKSPACE"
            }
        }
        // STAGE 2: Get the files from the GitLab repository (Define the branch name, the ssh url, the credentialsId..)
        stage('clone repo') {
            steps {
                script{
                    
                    echo " cloning from git"
                    try{
                       git credentialsId: "Sample-project",
                       url: "git@gitlab.eurecom.fr:trirematics/hydra.git",
                       branch: "podman"
                    } catch (err) {
                        throw(err)
                    }
                }
            }
        }
        // STAGE 3:
        stage('Cleaning-Parts') {
            steps {
                script{
                    if(SNAP_CLEAN == "true") {
                        dir ( "$SNAP_PATH" ){
                            sh '''
                                . ./hydra.env
                                SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                                echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                                echo "Cleaning the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                                snapcraft clean --provider $PROVIDER --bind-ssh $SNAPCRAFT_PARTS 
                                echo Cleaning the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful 
                            '''
                            
                        }
                    }
                }
            }
        }
        // STAGE 4:
        stage ('Pulling-Parts'){
            steps {
                 dir ( "$SNAP_PATH" ){
                    sh '''
                        . ./hydra.env
                        SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                        #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                        echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                        echo "Pulling the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                        snapcraft pull --provider $PROVIDER --bind-ssh $SNAPCRAFT_PARTS
                        echo "Pulling the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                 }
            }
        }  
        // STAGE 5:
         stage ('Building-Parts'){
            steps {
                dir ( "$SNAP_PATH" ){
                    sh '''
                    . ./hydra.env
                    SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                    #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                    echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                    echo "Building the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                    snapcraft build --provider $PROVIDER --bind-ssh $SNAPCRAFT_PARTS
                    echo "Building the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                }
            }
        }
        // STAGE 6:
        stage ('Staging-Parts'){
            steps {
                dir ( "$SNAP_PATH" ){
                    sh '''
                    . ./hydra.env
                    SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                    #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                    echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                    echo "Staging the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                    snapcraft stage --provider $PROVIDER --bind-ssh $SNAPCRAFT_PARTS
                    echo "Staging the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                }
            }
        }
        // STAGE 7:
        stage ('Priming-Parts'){
            steps {
                dir ( "$SNAP_PATH" ){
                    sh '''
                    . ./hydra.env
                    SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                    #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                    #echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                    echo "Priming the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                    snapcraft prime --provider $PROVIDER --bind-ssh $SNAPCRAFT_PARTS
                    echo "Priming the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                }
            }
        }
        // STAGE 8:
        stage ('Packaging-Parts'){
            steps {
                dir ( "$SNAP_PATH" ){
                    sh '''
                    . ./hydra.env
                    SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                    #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                    echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                    echo "Packaging the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                    snapcraft snap --provider $PROVIDER --bind-ssh 
                    echo "Packaging the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                }
            }
        }
        // STAGE 9:
        stage ('Extracting-Parts'){
            steps {
                dir ( "$SNAP_PATH" ){
                    sh '''
                    . ./hydra.env
                    SNAPCRAFT_PARTS=$(echo $SNAPCRAFT_PARTS |tr ',' ' ')
                    #SNAPCRAFT_PARTS=$(grep -m1 SNAPCRAFT_PARTS= hydra.env | cut -c17- | tr ',' ' ')
                    echo SNAPCRAFT_PARTS =  $SNAPCRAFT_PARTS
                    echo "Extracting the parts $SNAPCRAFT_PARTS  for $SNAP_NAME"
                    snapcraft try --provider $PROVIDER --bind-ssh
                    echo "Extracting the parts $SNAPCRAFT_PARTS  for $SNAP_NAME was successful "
                    '''
                }
            }
        }
        // STAGE 10:
        stage('install snap'){
            steps{
                dir ( "$SNAP_PATH" ){
                    sh '''
                    version=$(grep -m1 version: snap/snapcraft.yaml | cut -c9- | tr -d "'" | tr -d " ")
                    echo "$version"
                    sudo snap install --devmode  ./${SNAP_NAME}_${version}_multi.snap
                    '''
                }
                
            }
        }  
    }
    post {
        always {
             echo "You can always see me"
             echo "You can check your pipeline at ${env.BUILD_URL}"
         }
         success {
              echo " the Snap Building ran successfully"
         }
         failure {
             echo "OMG ! The Snap build failed"
         }
        //  cleanup {
        //     // clean up the workspace
        //     deleteDir()
        //     // remove the tmp directory
        //     dir("${workspace}@tmp") {
        //         deleteDir()
        //     }
        //  }
    }
}
